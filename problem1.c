/* Project Euler Problem 1
 *
 * Multiples of 3 and 5.
 */

#include<stdio.h>

int main() {
  int n = 0, i;

  for (i = 0; i < 1000; i++) {
    if (!(i % 3 && i % 5))
      n += i;
  }

  printf("%d\n", n);
  return 0;
}
