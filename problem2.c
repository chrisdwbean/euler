/* Project Euler Problem 2 (iterative)
 *
 * Even Fibonacci numbers.
 */

#include<stdio.h>

/* The upper limit for the fib sequence */
#define LIMIT 4000000
#define EVEN(n) n % 2 == 0

typedef unsigned int fib_t;

fib_t fib(int n) {
  fib_t fib_n = 1, fib_prev1 = 1, fib_prev2 = 1;
  
  while (n-- > 1) {
    fib_n = fib_prev1 + fib_prev2;
    fib_prev2 = fib_prev1;
    fib_prev1 = fib_n;
  }

  return fib_n;
}

int main() {
  fib_t n = 0, sum = 0;
  int i = 0;
  
  while ((n = fib(i++)) < LIMIT) {
    if (EVEN(n))
      sum += n;
  }
  
  printf("%lu\n", sum);
  
  return 0;
}
